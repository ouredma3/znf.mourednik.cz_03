<?php

namespace App\Model;

class UtilityModel extends BaseModel
{
    /** @var PidModel - model pro management rc*/
    private $pidModel;

    public function __construct(PidModel $pidModel)
    {
    $this->pidModel=$pidModel;
    }

    /**
     * Metoda detekuje pohlaví -1 = nedefinováno, 0 - žena, 1 - muž
     * @param int  $id rodného čísla
     */
    public function isMan($id)
    {
        if(!$id) return -1;
        $pid = $this->pidModel->getPid($id);
        if(!$pid) return -1;
        $rc = substr($pid['name'],2,2);
        return $rc<50;
    }

    /**
     * Metoda detekuje datum narození
     * @param int  $id rodného čísla
     */
    public function getBirthDay($id)
    {

        if(!$id) return null;
        $pid = $this->pidModel->getPid($id)['name'];
        $year=substr($pid,0,2);
        $month=substr($pid,2,2);
        $day=substr($pid,4,2);

        if($month>50){
            $month=$month-50;
        }

        if(strlen($pid)<10){
            $year="19".$year;
        }else{
            $year="20".$year;
        }
        return $day.".".$month.".".$year;
    }

}